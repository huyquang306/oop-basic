    <?php

    //Bài 1

    class ExerciseString
    {
        public $check1;
        public $check2;
        
        //Hàm đọc file
        function readFile($fileName)
        {
            $file = fopen($fileName,"r");
            $data = fread($file, filesize($fileName));
            fclose($file);

            return $data;
        }

        //Hàm kiểm tra file
        function checkValidString($str)
        {
            $str = " ".$str;

            return strpos($str, 'book') ^ strpos($str, 'restaurant');
        }

        //Hàm ghi file
        function writeFile($fileName, $data)
        {
            $file = fopen($fileName,"w");
            fwrite($file,$data);
            fclose($file);
        }
    }

    //Bài 2

    //Hàm đếm số câu
    function countSentences($str)
    {
        $sentences = explode($str, '.');
        $sentences = array_filter(array_map('trim',$sentences));

        return count($sentences);
    }

    //Code

    $object1 = new ExerciseString();

    //File 1

    $file1 = $object1->readFile("file1.txt"); 
    $object1->check1 = $object1->checkValidString($file1);

    //File2

    $file2 = $object1->readFile("file2.txt"); 
    $object1->check2 = $object1->checkValidString($file2);
    
    $result = "";

    //Kiểm tra check1

    $result .= "check1 là chuỗi ". (($object1->check1) ? "Hợp lệ <\br>" : "Không hợp lệ <\br>");

    //Kiểm tra check2

    $result .= "check2 là chuỗi ". (($object1->check2) ? "Hợp lệ <\br>" : "Không hợp lệ <\br>");
    $result .= "Chuỗi có ". countSentences($file2) ." câu <\br>";

    //Ghi vào file result

    $object1->writeFile("result_file.txt",$result);
