    <?php
    //Bài 3
    //Tạo abstract class

    abstract class Country
    {
        protected $slogan;
        abstract public function sayHello();
    }

    //Tạo interface 

    interface Boss
    {
        public function checkValidSlogan();
    }

    //Tạo trait

    trait Active
    {
        public function defindYourSelf()
        {
            return get_class();
        }
    }

    //Tạo class EnglandCountry

    class EnglandCountry extends Country implements Boss
    {
        use Active;

        public function setSlogan($str)
        {
            $this->slogan = $str;
        }
        
        public function checkValidSlogan()
        {
            $this->slogan = " ". $this->slogan;

            return strpos($this->slogan, "England") || strpos($this->slogan, "English");           
        }

        public function sayHello()
        {
            echo "Hello";
        }
    }

    //Tạo class VietnamCountry

    class VietnamCountry extends Country implements Boss
    {
        use Active;

        function setSlogan($str)
        {
            $this->slogan = $str;
        }
        
        function checkValidSlogan()
        {
            $this->slogan = " ". $this->slogan;

            return strpos($this->slogan, "vietnam") && strpos($this->slogan, "hust"); 
        }

        function sayHello()
        {
            echo "Xin Chao";
        }
    }

    
    $englandCountry = new EnglandCountry();
    $vietnamCountry = new VietnamCountry();

    $englandCountry->setSlogan('England is a country that is part of the United Kingdom. It shares land borders with Wales to the west and Scotland to the north. The Irish Sea lies west of England and the Celtic Sea to the southwest.');
    $vietnamCountry->setSlogan('Vietnam is the easternmost country on the Indochina Peninsula. With an estimated 94.6 million inhabitants as of 2016, it is the 15th most populous country in the world.');

    $englandCountry->sayHello(); //Hello
    echo "<br>";
    $vietnamCountry->sayHello(); //Xin Chao
    echo "<br>";
    var_dump($englandCountry->checkValidSlogan()); //True
    echo "<br>";
    var_dump($vietnamCountry->checkValidSlogan()); //False

    echo 'I am ' . $englandCountry->defindYourSelf(); 
    echo "<br>";
    echo 'I am ' . $vietnamCountry->defindYourSelf(); 
